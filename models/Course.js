// Setup dependencies
const mongoose = require("mongoose");

// Create the Schema
const courseSchema = new mongoose.Schema({

	name: {
		type: String,
		// Requires the data for this field/property to be included when creating a record.
		// the "true" value defines if the field is reqruired or not, and the second element in the array is the message we will display in the console when the data is not present
		required: [true, "Course Name is required"]
	},
	description: {
		type: String,
		required: [true, "Course description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		// The "new Date()" is an expression that instantiates a new "date" that stores the current date and time whenevera course is created in our database
	 	default: new Date()
	},
	// The "enrollees" property/field will be an array of objects containing the UserId and the date and time that the user enrolled 
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}

		}	
	]

});

//	model -> Course
//  collections -> courses
module.exports = mongoose.model("Course", courseSchema);

