const express = require("express");
const router = express.Router(); 
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js")

// Route for creating a course

/*
router.post("/create", (req, res) => {

courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));

})
*/



// Activity S39
// Route for creating a course with Authentication

router.post("/create", auth.verify, (req, res) => {

// Method # 1
	
	// contains all the information needed in the function
const data = {
	// data.course 
	course: req.body, // name, description, price
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}

	if (data.isAdmin) {
	courseController.addCourse(data.course).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false);
	}
});

/* Method # 2

	const userData = auth.decode(req.headers.authorization)

	courseController.addCourse(req.body, userData.isAdmin).then(resultFromController => res.send(resultFromController));

*/

/* Method # 2

	const userData = auth.decode(req.headers.authorization)

	courseController.addCourse(req.body, {userAdmin: userData.isAdmin).then(resultFromController => res.send(resultFromController));

*/



// Route for retrieveing all courses
router.get("/all", (req, res) => {

courseController.getAllCourses().then(resultFromController => res.send(resultFromController));

})


//  Route for retrieving all active courses
router.get("/active", (req, res) => {

	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));
})

// Route for Retrieving a specific course
router.get("/:courseId", (req, res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for Updating a course
router.put("/update/:courseId", auth.verify, (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params //best practice
	}

	if(data.isAdmin) {

		courseController.updateCourse(data.course, data.params).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false);
	}
	
})

// Other Method
/*
router.put("/update/:courseId", auth.verify, (req, req) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
		
	}

	if(data.isAdmin) {

		courseController.updateCourse(data.course, req.params).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false);
	}
	
})*/

// Activity S40
// Route for Archiving a Course
/*router.patch("/:courseId/archive", auth.verify, (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		courseController.archiveCourse(data.course, data.params).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false);
	}
	
})
*/


// Route for Archiving a Course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases

router.put("/:courseId/archive", auth.verify, (req, res) => {
	
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		courseController.archiveCourse(data.params).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false);
	}
	
})


module.exports = router;